<h1><i>X-Men</i> (2000)/Home media</h1>
<h2>North American releases</h2>
<table style="width: 100%;">
<tbody>
<tr>
<th>Format</th>
<th>Cover</th>
<th>Release date(s)</th>
<th>Publisher</th>
<th>Notes</th>
</tr>
<tr>
<td>VHS</td>
<td><img src="https://images.45worlds.com/f/dv/xmen-74-dv.jpg" alt="" width="150"></td>
<td>November 21, 2000</td>
<td>20th Century Fox Home Entertainment</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>DVD</td>
<td><img src="https://images.45worlds.com/f/dv/xmen-25-dv.jpg" alt="" width="150"></td>
<td>November 21, 2000</td>
<td>20th Century Fox Home Entertainment</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>Blu-ray</td>
<td><img src="https://images.static-bluray.com/movies/covers/3736_front.jpg" width="150px"></td>
<td>April 21, 2009</td>
<td>20th Century Fox Home Entertainment</td>
<td>2-Disc Edition</td>
</tr>
</tbody>
</table>